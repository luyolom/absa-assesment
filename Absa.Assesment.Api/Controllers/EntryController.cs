﻿using Absa.Assesment.Api.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Absa.Assesment.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class EntryController : ControllerBase
    {

        private readonly ILogger<EntryController> _logger;
        private readonly AbsaAssesmentContext _context;

        public EntryController(ILogger<EntryController> logger, AbsaAssesmentContext context)
        {
            _logger = logger;
            _context = context;
        }

        [HttpGet("GetEntry/{id}")]
        public async Task<ActionResult<Entry>> GetEntryById(int id)
        {
            try
            {
                return await _context.Entries.FindAsync(id);
            }
            catch (Exception ex)
            {
                _logger.LogError("An Error occured", ex);
            }
            return NotFound();
        }

        [HttpPost("CreateEntry")]
        public async Task<ActionResult<Entry>> PostEntry(Entry entry)
        {
            try
            {
                _context.Entries.Add(entry);
                await _context.SaveChangesAsync();

                return CreatedAtAction("GetEntry", new { id = entry.Id }, entry);
            }
            catch(Exception ex)
            {
                _logger.LogError("An Error occured", ex);
            }
            return NotFound();
        }

        [HttpGet("GetEntriesByPhonebookId/{id}")]
        public async Task<ActionResult<IEnumerable<Entry>>> GetEntriesByPhonebookId(int id)
        {
            try
            {
                return await _context.Entries.Where(x => x.PhoneBookId == id)
                                .ToListAsync();

            }
            catch (Exception ex)
            {
                _logger.LogError("An Error occured", ex);
            }
            return NotFound();
        }
    }
}
