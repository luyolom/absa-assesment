﻿using Absa.Assesment.Api.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Absa.Assesment.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PhoneBookController : ControllerBase
    {

        private readonly ILogger<PhoneBookController> _logger;
        private readonly AbsaAssesmentContext _context;
        public PhoneBookController(ILogger<PhoneBookController> logger, AbsaAssesmentContext context)
        {
            _logger = logger;
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<PhoneBook>>> GetPhoneBook()
        {
            try
            {
                return await _context.PhoneBooks.ToListAsync();
            }
            catch(Exception ex)
            {
                _logger.LogError("An Error occured", ex);
            }
            return NotFound();
        }


        [HttpPost("CreatePhoneBook")]
        public async Task<ActionResult<PhoneBook>> PostPhoneBook(PhoneBook phoneBook)
        {
            try
            {
                _context.PhoneBooks.Add(phoneBook);
                await _context.SaveChangesAsync();

                return CreatedAtAction("GetPhoneBook", new { id = phoneBook.Id }, phoneBook);
            }
            catch (Exception ex)
            {
                _logger.LogError("An Error occured", ex);
            }
            return NotFound();
        }

        [HttpGet("GetPhoneBook/{id}")]
        public async Task<ActionResult<PhoneBook>> GetPhoneBook(int id)
        {
            try
            {
                return await _context.PhoneBooks.FindAsync(id);
            }
            catch (Exception ex)
            {
                _logger.LogError("An Error occured", ex);
            }
            return NotFound();
        }

    }
}
