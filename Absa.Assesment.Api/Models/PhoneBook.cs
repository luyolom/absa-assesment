﻿using System;
using System.Collections.Generic;



namespace Absa.Assesment.Api.Models
{
    public partial class PhoneBook
    {
        public PhoneBook()
        {
            Entries = new HashSet<Entry>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Entry> Entries { get; set; }
    }
}
