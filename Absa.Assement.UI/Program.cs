using Absa.Assement.UI.Services;
using Absa.Assement.UI.ViewModels;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace Absa.Assement.UI
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("#app");

            var defaultApi = builder.Configuration.GetValue<string>("ApiUrl:DefaultApi");

            builder.Services.AddSingleton(client => new HttpClient { BaseAddress = new Uri(defaultApi) });
            builder.Services.AddSingleton<IPhoneBookService<PhoneBook>, PhoneBookService<PhoneBook>>();
            builder.Services.AddSingleton<IPhoneBookService<Entry>, PhoneBookService<Entry>>();
            await builder.Build().RunAsync();
        }
    }
}
