﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Absa.Assement.UI.ViewModels
{
    public class PhoneBook
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
