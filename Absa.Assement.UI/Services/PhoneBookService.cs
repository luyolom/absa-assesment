﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Absa.Assement.UI.Services
{
    public class PhoneBookService<T> : IPhoneBookService<T>
    {
        public HttpClient _httpClient { get; }
        public PhoneBookService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }
        public async Task<List<T>> GetAllAsync(string requestUri)
        {
            var response = await _httpClient.GetAsync(requestUri);
            var responseStatusCode = response.StatusCode;

            if (responseStatusCode.ToString() == "OK")
            {
                var responseBody = await response.Content.ReadAsStringAsync();
                return await Task.FromResult(JsonConvert.DeserializeObject<List<T>>(responseBody));
            }
            else
                return null;
        }

        public async Task<T> SaveAsync(string requestUri, T obj)
        {
            var requestMessage = new HttpRequestMessage(HttpMethod.Post, requestUri);
            string serialisedObject = JsonConvert.SerializeObject(obj);
            requestMessage.Content = new StringContent(serialisedObject);
            requestMessage.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            var response = await _httpClient.SendAsync(requestMessage);
            var responseBody = await response.Content.ReadAsStringAsync();

            var returnedObj = JsonConvert.DeserializeObject<T>(responseBody);

            return await Task.FromResult(returnedObj);
        }

        public async Task<List<T>> GetAllByIdAsync(string requestUri, int Id)
        {
            var response = await _httpClient.GetAsync($"{requestUri}/{Id}");
            var responseStatusCode = response.StatusCode;

            if (responseStatusCode.ToString() == "OK")
            {
                var responseBody = await response.Content.ReadAsStringAsync();
                return await Task.FromResult(JsonConvert.DeserializeObject<List<T>>(responseBody));
            }
            else
                return null;
        }

        public async Task<T> GetByIdAsync(string requestUri, int Id)
        {
            var response = await _httpClient.GetAsync($"{requestUri}/{Id}");
            var responseStatusCode = response.StatusCode;

            if (responseStatusCode.ToString() == "OK")
            {
                var responseBody = await response.Content.ReadAsStringAsync();
                return await Task.FromResult(JsonConvert.DeserializeObject<T>(responseBody));
            }
            else
                return default(T);
        }

    }
}
