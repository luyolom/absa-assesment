﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Absa.Assement.UI.Services
{
    interface IPhoneBookService<T>
    {
        Task<List<T>> GetAllAsync(string requestUri);
        Task<List<T>> GetAllByIdAsync(string requestUri, int Id);
        Task<T> GetByIdAsync(string requestUri, int Id);
        Task<T> SaveAsync(string requestUri, T obj);
    }
}
